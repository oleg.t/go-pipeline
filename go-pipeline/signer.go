package go_pipeline

import (
	"strconv"
	"strings"
)

// сюда писать код
func ExecutePipeline(freeFlowJobs []job) {

}

func SingleHash(in chan interface{}, out chan interface{}) {
	data := <-in
	val, ok1 := data.(string)
	if !ok1 {
		val = strconv.FormatInt(int64(data.(int)), 10)
	}
	results := DataSignerCrc32(val)
	results += "~"
	results += DataSignerCrc32(DataSignerMd5(val))
	out <- results
	return
}

func MultiHash(in chan interface{}, out chan interface{}) {
	thSlice := []int{0, 1, 2, 3, 4, 5}

	data := <-in
	initVal, ok1 := data.(string)
	if !ok1 {
		initVal = string(data.(int))
	}

	results := make([]string, 6)
	for th := range thSlice {
		results = append(results, DataSignerCrc32(string(th)+initVal))
	}
	out <- strings.Join(results, "")
}

func CombineResults(in chan interface{}, out chan interface{}) {

}

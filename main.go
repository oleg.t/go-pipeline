package main

import (
	"fmt"
	"go-pipeline/go-pipeline"
)

func main() {
	//Результаты не совпадают, что то не так :\
	in := make(chan interface{}, 1)
	out := make(chan interface{}, 1)
	in <- 1
	go_pipeline.SingleHash(in, out)
	in <- <-out
	go_pipeline.MultiHash(in, out)
	fmt.Print(<-out)
	//<- out
}
